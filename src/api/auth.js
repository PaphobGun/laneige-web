import client from 'utils/client'
import { formatError } from 'utils'

const BASE_ENDPOINT = '/auth'

const postRegister = async (registerForm) => {
  try {
    const res = await client.post(BASE_ENDPOINT + '/register', registerForm)
    return { json: res.data }
  } catch (err) {
    return formatError(err)
  }
}

const postLogin = async (loginForm) => {
  try {
    const res = await client.post(BASE_ENDPOINT + '/login', loginForm)
    return { json: res.data }
  } catch (err) {
    return formatError(err)
  }
}

export default { postRegister, postLogin }
