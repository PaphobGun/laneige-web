import client from 'utils/client'
import { formatError } from 'utils'

const BASE_ENDPOINT = '/projects'

const postCreateProject = async (projectForm) => {
  try {
    const res = await client.post(BASE_ENDPOINT, projectForm)
    return { json: res.data }
  } catch (err) {
    return formatError(err)
  }
}

const getAllProjects = async () => {
  try {
    const res = await client.get(BASE_ENDPOINT)
    return { json: res.data }
  } catch (err) {
    return formatError(err)
  }
}

export default { postCreateProject, getAllProjects }
