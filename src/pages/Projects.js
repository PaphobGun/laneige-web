import React, { useState, useEffect } from 'react'
import { Typography, Container, Grid } from '@material-ui/core'
import ProjectCard from 'modules/Project/ProjectCard'
import withRequireAuth from 'utils/withRequireAuth'
import withNavbar from 'modules/Navbar/withNavbar'
import CreateProjectModal from 'modules/Project/CreateProjectModal'
import { useSelector, useDispatch } from 'react-redux'
import { showLoading, disableLoading } from 'modules/WithLoading/action'
import styled from 'styled-components'
import Btn from 'components/Btn'
import useIsDesktop from 'utils/useIsDesktop'
import { fetchProjects, initOnPageProjects } from 'modules/Project/action'

const RenderProjectCards = ({ projects }) =>
  projects.map((project, i) => (
    <Grid key={`project-${i}`} item justify='center'>
      <ProjectCard project={project} />
    </Grid>
  ))

const Projects = () => {
  const [openModal, setOpenModal] = useState(false)
  const dispatch = useDispatch()
  const { isCreating, projects } = useSelector((state) => state.project)
  const isDesktop = useIsDesktop()
  const toggleModal = () => {
    setOpenModal(!openModal)
  }

  useEffect(() => {
    const isLoading = isCreating
    if (isLoading) dispatch(showLoading())
    else dispatch(disableLoading())
  }, [dispatch, isCreating])

  useEffect(() => {
    dispatch(initOnPageProjects())
    dispatch(fetchProjects())
  }, [dispatch])

  return (
    <Container style={{ position: 'relative' }}>
      {openModal && <CreateProjectModal onClose={toggleModal} />}
      <Typography variant='h5' color='textPrimary'>
        Projects
      </Typography>
      <CreateProjectBtn onClick={toggleModal} variant='outlined' color='primary'>
        create
      </CreateProjectBtn>
      <CardWrapper isDesktop={isDesktop} container spacing={4}>
        <RenderProjectCards projects={projects} />
      </CardWrapper>
    </Container>
  )
}

const CreateProjectBtn = styled(Btn)`
  top: 0;
  right: 0;
  position: absolute;
`

const CardWrapper = styled(Grid)`
  flex-grow: 1;
  margin-top: 24px;
  justify-content: ${({ isDesktop }) => (isDesktop ? 'flex-start' : 'center')};
`

export default withRequireAuth(withNavbar(Projects))
