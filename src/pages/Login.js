import React, { useEffect, useCallback } from 'react'
import styled from 'styled-components'
import { Typography } from '@material-ui/core'
import LoginForm from 'modules/Auth/LoginForm'
import { useDispatch, useSelector } from 'react-redux'
import { initialOnPage } from 'modules/Auth/action'
import { showLoading, disableLoading } from 'modules/WithLoading/action'
import ErrorModal from 'components/ErrorModal'
import withNoToken from 'utils/withNoToken'

const Login = () => {
  const dispatch = useDispatch()
  const { isFetching, isServerErr } = useSelector((state) => state.auth)
  const initialPage = useCallback(() => {
    dispatch(initialOnPage())
  }, [dispatch])

  useEffect(() => {
    const isLoading = isFetching
    if (isLoading) dispatch(showLoading())
    else dispatch(disableLoading())
  }, [dispatch, isFetching])

  useEffect(() => {
    initialPage()
  }, [initialPage])

  return (
    <>
      {isServerErr && <ErrorModal onClose={initialPage} />}
      <Wrapper>
        <Typography className='login-title' variant='h4' color='textPrimary'>
          Login
        </Typography>
        <LoginForm />
      </Wrapper>
    </>
  )
}

const Wrapper = styled.div`
  position: absolute;
  top: 40%;
  left: 50%;
  width: 320px;
  height: 425px;
  display: flex;
  box-sizing: border-box;
  padding: 16px 32px 0 32px;
  flex-direction: column;
  transform: translate(-50%, -50%);

  .login-title {
    margin-bottom: 54px;
  }
`

export default withNoToken(Login)
