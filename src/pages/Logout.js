import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { logout } from 'modules/Auth/action'

const Logout = () => {
  const dispatch = useDispatch()
  const { isLogin } = useSelector((state) => state.auth)
  const history = useHistory()

  useEffect(() => {
    dispatch(logout())
  }, [dispatch])

  useEffect(() => {
    if (!isLogin) history.push('/login')
  }, [history, isLogin])

  return <div>Logout</div>
}

export default Logout
