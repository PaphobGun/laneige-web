import React from 'react'
import withRequireAuth from 'utils/withRequireAuth'
import withNavbar from 'modules/Navbar/withNavbar'
import { Typography, Container } from '@material-ui/core'

const Home = () => {
  return (
    <Container>
      <Typography variant='h5' color='textPrimary'>
        Home
      </Typography>
    </Container>
  )
}

export default withRequireAuth(withNavbar(Home))
