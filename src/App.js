import React from 'react'
import { Router, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'

import Home from 'pages/Home'
import Login from 'pages/Login'

import store from 'store'
import history from 'utils/history'

import WithLoading from 'modules/WithLoading'
import theme from 'components/theme'
import { MuiThemeProvider } from '@material-ui/core'
import WithToken from 'utils/WithToken'
import Projects from 'pages/Projects'
import Logout from 'pages/Logout'

function App() {
  return (
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <WithToken>
          <WithLoading>
            <Router history={history}>
              <Switch>
                <Route path='/' exact component={Home} />
                <Route path='/projects' component={Projects} />
                <Route path='/login' component={Login} />
                <Route path='/logout' component={Logout} />
              </Switch>
            </Router>
          </WithLoading>
        </WithToken>
      </MuiThemeProvider>
    </Provider>
  )
}

export default App
