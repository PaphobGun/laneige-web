import React from 'react'
import { makeStyles } from '@material-ui/core'
import Navbar from '.'

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
    },
  },
  nav: {
    flexShrink: 0,
  },
  content: {
    flexGrow: 1,
    boxSizing: 'border-box',
    padding: theme.spacing(4),
  },
}))

const withNavbar = (WrappedComponent) => (props) => {
  const styles = useStyles()
  return (
    <div className={styles.root}>
      <Navbar className={styles.nav} />
      <div className={styles.content}>
        <WrappedComponent {...props} />
      </div>
    </div>
  )
}

export default withNavbar
