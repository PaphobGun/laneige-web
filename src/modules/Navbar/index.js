import React, { useState } from 'react'
import styled, { css, keyframes } from 'styled-components'
import { List, Drawer, Typography, Box, Icon, fade } from '@material-ui/core'
import { Menu } from '@material-ui/icons'
import { useHistory } from 'react-router-dom'
import { ROUTES } from 'configs'
import { PALETTE } from 'components/theme'
import { Linked } from 'components/Linked'
import useIsDesktop from 'utils/useIsDesktop'
import Item from 'components/Item'

const RenderItem = ({ route, isActive }) => {
  const iconColor = isActive ? 'primary' : 'disabled'
  const textColor = isActive ? 'textPrimary' : 'textSecondary'
  return (
    <Linked to={route.path}>
      <MenuItem isActive={isActive}>
        <Box display='flex' alignItems='center'>
          <Icon component={route.icon} color={iconColor} fontSize='large' />
          <Box ml={2}>
            <Typography color={textColor}>{route.title}</Typography>
          </Box>
        </Box>
      </MenuItem>
    </Linked>
  )
}

const Sidebar = ({ path }) => {
  return (
    <SideNav>
      <List>
        {ROUTES.map((route, i) => (
          <RenderItem key={`menu-${i}`} route={route} isActive={path === route.path} />
        ))}
      </List>
    </SideNav>
  )
}

const Navbar = ({ className }) => {
  const [mobileOpen, setMobileOpen] = useState(false)
  const handleToggle = () => {
    setMobileOpen(!mobileOpen)
  }
  const { location } = useHistory()
  const isDesktop = useIsDesktop()

  return (
    <div className={className}>
      {isDesktop && <Sidebar path={location.pathname} />}

      {!isDesktop && (
        <div>
          <MenuIcon onClick={handleToggle} />
          <Drawer
            open={mobileOpen}
            onClose={handleToggle}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            <Sidebar path={location.pathname} />
          </Drawer>
        </div>
      )}
    </div>
  )
}

const activeAnimation = keyframes`
  from {
    padding-left: 0;
    background-color: white;
  }
  to{
    padding-left: 24px;
    background-color: ${fade(PALETTE.primary.main, 0.1)};
  }
`

const isActiveStyle = css`
  padding-left: 24px;
  animation: ${activeAnimation} ease-out 0.3s;
  box-shadow: 4px 0 ${PALETTE.primary.main};
  background-color: ${fade(PALETTE.primary.main, 0.1)};
  border-radius: 4px;
`

const MenuItem = styled(Item)`
  padding: 16px;
  ${({ isActive }) => (isActive ? isActiveStyle : '')};
`

const MenuIcon = styled(Menu)`
  padding: 8px;
  cursor: pointer;
`

const SideNav = styled.div`
  height: 100vh;
  min-width: 240px;
  background-color: white;
  top: 0;
  left: 0;
  position: sticky;
  box-shadow: 2px 0 8px #ddeaef;
`

export default Navbar
