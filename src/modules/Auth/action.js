import {
  LOGIN_CLIENT_ERR,
  LOGIN_SERVER_ERR,
  REGISTER_CLIENT_ERR,
  REGISTER_SERVER_ERR,
  REGISTER_FETCHING,
  REGISTER_SUCCESS,
  LOGIN_FETCHING,
  LOGIN_SUCCESS,
  INITIAL_ON_PAGE,
  LOGOUT,
} from './reducer'
import api from 'api/auth'
import localToken from 'utils/localToken'

export const login = (loginForm) => async (dispatch) => {
  dispatch({ type: LOGIN_FETCHING })
  const { err, json } = await api.postLogin(loginForm)
  if (err) {
    const { isClientErr, errMsg } = err
    if (isClientErr) dispatch({ type: LOGIN_CLIENT_ERR, errMsg })
    else dispatch({ type: LOGIN_SERVER_ERR })
  } else {
    localToken.setToken(json.data)
    dispatch({ type: LOGIN_SUCCESS })
  }
}

export const register = (registerForm) => async (dispatch) => {
  dispatch({ type: REGISTER_FETCHING })
  const { passwordConfirmation, ...cleanedForm } = registerForm
  const { err, json } = await api.postRegister(cleanedForm)
  if (err) {
    const { isClientErr, errMsg } = err
    if (isClientErr) dispatch({ type: REGISTER_CLIENT_ERR, errMsg })
    else dispatch({ type: REGISTER_SERVER_ERR })
  } else {
    localToken.setToken(json)
    dispatch({ type: REGISTER_SUCCESS })
  }
}

export const logout = () => {
  localToken.clearToken()
  return { type: LOGOUT }
}

export const initialOnPage = () => {
  return { type: INITIAL_ON_PAGE }
}
