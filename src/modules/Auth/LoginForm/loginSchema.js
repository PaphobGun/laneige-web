import * as Yup from 'yup'

export const loginSchema = Yup.object({
  accountUsername: Yup.string().min(6, 'Minimum 6 characters').required('Required'),
  accountPassword: Yup.string().min(8, 'Minimum 8 characters').required('Required'),
})
