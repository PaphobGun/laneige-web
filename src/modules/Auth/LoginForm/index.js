import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useFormik } from 'formik'
import { loginSchema } from './loginSchema'
import { login } from '../action'
import styled from 'styled-components'
import ErrorMessage from 'components/ErrorMessage'
import { Button } from '@material-ui/core'
import InputForm from 'components/InputForm'

const INITIAL_LOGIN_FORM = {
  accountUsername: '',
  accountPassword: '',
}

const LoginForm = () => {
  const dispatch = useDispatch()
  const { errMsg } = useSelector((state) => state.auth)
  const { values, handleSubmit, handleChange, errors, touched, handleBlur } = useFormik({
    initialValues: INITIAL_LOGIN_FORM,
    validationSchema: loginSchema,
    onSubmit: () => {
      dispatch(login(values))
    },
  })

  return (
    <Form onSubmit={handleSubmit}>
      <InputForm
        className='login-input'
        name='accountUsername'
        placeholder='username'
        onChange={handleChange}
        value={values.accountUsername}
        error={errors.accountUsername}
        touched={touched.accountUsername}
        onBlur={handleBlur}
      />
      <InputForm
        className='login-input'
        type='password'
        name='accountPassword'
        placeholder='password'
        onChange={handleChange}
        value={values.accountPassword}
        error={errors.accountPassword}
        touched={touched.accountPassword}
        onBlur={handleBlur}
      />
      <ErrorMessage className='login-error' message={errMsg} />
      <Button size='large' fullWidth type='submit' variant='contained' color='primary'>
        Login
      </Button>
    </Form>
  )
}

const Form = styled.form`
  .login-error {
    margin-top: 54px;
  }

  .login-input {
    margin-bottom: 8px;
  }
`

export default LoginForm
