const INITIAL_STATE = {
  user: {},
  errMsg: '',
  isLogin: false,
  isFetching: false,
  isServerErr: false,
  isInitial: false,
}

export const LOGIN_FETCHING = 'AUTH/LOGIN_FETCHING'
export const LOGIN_SUCCESS = 'AUTH/LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'AUTH/LOGIN_FAILURE'
export const LOGIN_CLIENT_ERR = 'AUTH/LOGIN_CLIENT_ERR'
export const LOGIN_SERVER_ERR = 'AUTH/LOGIN_SERVER_ERR'

export const REGISTER_FETCHING = 'AUTH/REGISTER_FETCHING'
export const REGISTER_SUCCESS = 'AUTH/REGISTER_SUCCESS'
export const REGISTER_CLIENT_ERR = 'AUTH/REGISTER_CLIENT_ERR'
export const REGISTER_SERVER_ERR = 'AUTH/REGISTER_SERVER_ERR'

export const LOGOUT = 'AUTH/LOGOUT'

export const INITIAL_ON_PAGE = 'AUTH/INITIAL_ON_PAGE'

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN_FETCHING:
      return { ...state, isFetching: true }
    case LOGIN_SUCCESS:
      return { ...state, isLogin: true, isFetching: false, errMsg: '', isServerErr: false, isInitial: true }
    case LOGIN_FAILURE:
      return { ...state, isLogin: false, isFetching: false, errMsg: '', isServerErr: false, isInitial: true }
    case LOGIN_CLIENT_ERR:
      return { ...state, isFetching: false, errMsg: action.errMsg }
    case LOGIN_SERVER_ERR:
      return { ...state, isFetching: false, isServerErr: true }

    case REGISTER_FETCHING:
      return { ...state, isFetching: true }
    case REGISTER_SUCCESS:
      return { ...state, isLogin: true, isFetching: false, errMsg: '', isServerErr: false }
    case REGISTER_CLIENT_ERR:
      return { ...state, isFetching: false, errMsg: action.errMsg }
    case REGISTER_SERVER_ERR:
      return { ...state, isFetching: false, isServerErr: true }

    case LOGOUT:
      return { ...state, isLogin: false }

    case INITIAL_ON_PAGE:
      return { ...state, errMsg: '', isServerErr: false }

    default:
      return state
  }
}
