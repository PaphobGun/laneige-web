import * as Yup from 'yup'

export const createProjectSchema = Yup.object({
  projectName: Yup.string().required('Required'),
})
