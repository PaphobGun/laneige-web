const INITIAL_STATE = {
  projects: [],
  errMsg: '',
  isCreating: false,
  isServerErr: false,
}

export const PROJECT_CREATING = 'PROJECT/CREATE_PROJECT'
export const PROJECT_CREATE_SUCCESS = 'PROJECT/CREATE_PROJECT_SUCCESS'

export const PROJECT_FETCHING = 'PROJECT/PROJECT_FETCHING '
export const PROJECT_FETCH_SUCCESS = 'PROJECT/PROJECT_FETCH_SUCCESS'

export const PROJECT_CLIENT_ERR = 'PROJECT/CLIENT_ERR'
export const PROJECT_SERVER_ERR = 'PROJECT/SERVER_ERR'

export const INITIAL_ON_PAGE = 'PROJECT/INITIAL_ON_PAGE'

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PROJECT_FETCHING:
      return { ...state, isCreating: true }
    case PROJECT_FETCH_SUCCESS:
      return {
        ...state,
        isCreating: false,
        errMsg: '',
        isServerErr: false,
        projects: action.projects,
      }

    case PROJECT_CREATING:
      return { ...state, isCreating: true }
    case PROJECT_CREATE_SUCCESS:
      return {
        ...state,
        isCreating: false,
        errMsg: '',
        isServerErr: false,
        projects: [...state.projects, action.project],
      }
    case PROJECT_CLIENT_ERR:
      return { ...state, isCreating: false, errMsg: action.errMsg }
    case PROJECT_SERVER_ERR:
      return { ...state, isCreating: false, isServerErr: true }

    case INITIAL_ON_PAGE:
      return INITIAL_STATE

    default:
      return state
  }
}
