import api from 'api/project'
import {
  PROJECT_CREATE_SUCCESS,
  PROJECT_CREATING,
  PROJECT_CLIENT_ERR,
  PROJECT_SERVER_ERR,
  PROJECT_FETCHING,
  PROJECT_FETCH_SUCCESS,
  INITIAL_ON_PAGE,
} from './reducer'

export const createProject = (projectForm) => async (dispatch) => {
  dispatch({ type: PROJECT_CREATING })
  const { err, json } = await api.postCreateProject(projectForm)
  if (err) {
    const { isClientErr, errMsg } = err
    if (isClientErr) dispatch({ type: PROJECT_CLIENT_ERR, errMsg })
    else dispatch({ type: PROJECT_SERVER_ERR })
  } else {
    dispatch({ type: PROJECT_CREATE_SUCCESS, project: json.data })
  }
}

export const fetchProjects = () => async (dispatch) => {
  dispatch({ type: PROJECT_FETCHING })
  const { err, json } = await api.getAllProjects()
  if (err) {
    const { isClientErr, errMsg } = err
    if (isClientErr) dispatch({ type: PROJECT_CLIENT_ERR, errMsg })
    else dispatch({ type: PROJECT_SERVER_ERR })
  } else {
    dispatch({ type: PROJECT_FETCH_SUCCESS, projects: json.data })
  }
}

export const initOnPageProjects = () => ({ type: INITIAL_ON_PAGE })
