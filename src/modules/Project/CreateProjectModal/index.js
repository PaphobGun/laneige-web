import React from 'react'
import { Typography, MenuItem } from '@material-ui/core'
import Modal from 'components/Modal'
import { useFormik } from 'formik'
import { createProjectSchema } from '../projectSchema'
import InputForm from 'components/InputForm'
import Btn from 'components/Btn'
import { useModalStyles } from 'components/Modal/useModalStyles'
import { Close } from '@material-ui/icons'
import { useDispatch } from 'react-redux'
import { createProject } from '../action'

const INITIAL_VALUES = {
  projectName: '',
}

const CreateProjectModal = ({ onClose }) => {
  const dispatch = useDispatch()

  const { handleSubmit, handleChange, values, errors, touched, handleBlur } = useFormik({
    initialValues: INITIAL_VALUES,
    validationSchema: createProjectSchema,
    onSubmit: () => {
      dispatch(createProject(values))
      onClose()
    },
  })

  const modalClasses = useModalStyles()

  return (
    <Modal onClose={onClose}>
      <Typography className={modalClasses.title} variant='h5' color='textPrimary'>
        Create new project
      </Typography>
      <MenuItem onClick={onClose} className={modalClasses.closeWrapper}>
        <Close className={modalClasses.closeIcon} />
      </MenuItem>
      <form onSubmit={handleSubmit}>
        <InputForm
          name='projectName'
          placeholder='project name'
          onChange={handleChange}
          value={values.projectName}
          error={errors.projectName}
          touched={touched.projectName}
          onBlur={handleBlur}
        />
        <div className={modalClasses.btnGroup}>
          <Btn onClick={onClose} variant='outlined' color='primary'>
            cancel
          </Btn>
          <Btn type='submit' variant='contained' color='primary'>
            create
          </Btn>
        </div>
      </form>
    </Modal>
  )
}

export default CreateProjectModal
