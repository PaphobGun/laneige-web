import React from 'react'
import { makeStyles, fade } from '@material-ui/core'
import { Linked } from 'components/Linked'
import Item from 'components/Item'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '320px',
    height: '200px',
    backgroundColor: 'white',
    borderRadius: '4px',
    color: theme.palette.text.primary,
    boxShadow: `4px 8px 32px ${fade(theme.palette.primary.main, 0.2)}`,
  },
}))

const ProjectCard = ({ project }) => {
  const classes = useStyles()
  return (
    <Linked>
      <Item className={classes.root}>{project.projectName}</Item>
    </Linked>
  )
}

export default ProjectCard
