const INITIAL_STATE = {
  isLoading: false,
}

export const SHOW_LOADING = 'LOADING_SHOW'

export const DISABLE_LOADING = 'LOADING_DISABLE'

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SHOW_LOADING:
      return { ...state, isLoading: true }
    case DISABLE_LOADING:
      return { ...state, isLoading: false }
    default:
      return state
  }
}
