import { SHOW_LOADING, DISABLE_LOADING } from './reducer'

export const showLoading = () => {
  return { type: SHOW_LOADING }
}

export const disableLoading = () => {
  return { type: DISABLE_LOADING }
}
