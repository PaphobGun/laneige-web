import React from 'react'
import { useSelector } from 'react-redux'
import { CircularProgress } from '@material-ui/core'
import styled from 'styled-components'

const WithLoading = ({ children }) => {
  const { isLoading } = useSelector((state) => state.loading)
  return (
    <div>
      {isLoading && (
        <div>
          <Loading>
            <CircularProgress />
            <h4>loading</h4>
          </Loading>
          <Overlay />
        </div>
      )}
      {children}
    </div>
  )
}

const Overlay = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: white;
  opacity: 0.5;
  z-index: 2;
`

const Loading = styled.div`
  top: 50%;
  left: 50%;
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 3;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  transform: translate(-50%, -50%);
`

export default WithLoading
