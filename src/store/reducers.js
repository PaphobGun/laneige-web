// Reducers should be here
import { combineReducers } from 'redux'
import auth from 'modules/Auth/reducer'
import loading from 'modules/WithLoading/reducer'
import project from 'modules/Project/reducer'

export default combineReducers({ auth, loading, project })
