import { Home, Work, ExitToApp } from '@material-ui/icons'

export const BASE_URL = process.env.REACT_APP_BASE_URL

export const ROUTES = [
  { path: '/', title: 'Home', icon: Home },
  { path: '/projects', title: 'Projects', icon: Work },
  { path: '/logout', title: 'Logout', icon: ExitToApp },
]
