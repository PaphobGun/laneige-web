import React from 'react'
import { TextField } from '@material-ui/core'
import styled from 'styled-components'

const InputForm = ({ name, onChange, value, error, touched, ...rest }) => {
  const isError = touched && !!error
  return (
    <Wrapper>
      <TextField
        variant='outlined'
        fullWidth
        className='login-input'
        name={name}
        placeholder={name}
        onChange={onChange}
        value={value}
        error={isError}
        helperText={isError && error}
        {...rest}
      />
    </Wrapper>
  )
}

const Wrapper = styled.div`
  box-sizing: border-box;
  margin-bottom: 24px;
  height: 64px;
`

export default InputForm
