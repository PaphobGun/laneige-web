import React from 'react'
import { Modal as MaterialModal, makeStyles, MenuItem, Fade, Backdrop } from '@material-ui/core'
import { blueGrey, grey } from '@material-ui/core/colors'
import { Close } from '@material-ui/icons'

const useStyles = makeStyles((theme) => ({
  content: {
    position: 'fixed',
    top: '50%',
    left: '50%',
    width: '320px',
    minHeight: '175px',
    maxHeight: '425px',
    padding: '16px 24px 16px 24px',
    zIndex: 20,
    backgroundColor: 'white',
    boxShadow: `0px 8px 32px ${blueGrey[800]}`,
    borderRadius: '4px',
    transform: 'translate(-50%, -50%)',
  },
  closeWrapper: {
    top: 0,
    right: 0,
    padding: theme.spacing(2),
    borderRadius: '50%',
    position: 'absolute',
  },
  closeIcon: {
    color: grey[400],
  },
}))

const Modal = ({ children, onClose }) => {
  const [open, setOpen] = React.useState(true)
  const classes = useStyles()
  const handleClose = () => {
    onClose()
    setOpen(false)
  }

  if (!open) return null
  return (
    <MaterialModal
      open={open}
      onClose={handleClose}
      disableAutoFocus
      disableEnforceFocus
      style={{ position: 'relative' }}
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 100,
      }}
    >
      <Fade in={open}>
        <div className={classes.content}>
          <MenuItem onClick={handleClose} className={classes.closeWrapper}>
            <Close className={classes.closeIcon} />
          </MenuItem>
          {children}
        </div>
      </Fade>
    </MaterialModal>
  )
}

export default Modal
