import { makeStyles } from '@material-ui/core'
import { grey } from '@material-ui/core/colors'

export const useModalStyles = makeStyles((theme) => ({
  closeWrapper: {
    top: 0,
    right: 0,
    padding: theme.spacing(2),
    borderRadius: '50%',
    position: 'absolute',
  },
  closeIcon: {
    color: grey[500],
  },
  title: {
    marginBottom: theme.spacing(4),
  },
  // form: {
  //   marginBottom: theme.spacing(3),
  // },
  btnGroup: {
    textAlign: 'right',
    '& > button:not(:last-child)': {
      marginRight: theme.spacing(1),
    },
  },
  // description: {
  //   marginBottom: theme.spacing(4),
  // },
}))
