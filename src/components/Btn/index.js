import React from 'react'
import { Button as MaterialBtn, fade } from '@material-ui/core'
import styled, { css } from 'styled-components'
import { red } from '@material-ui/core/colors'

const Btn = ({ color, variant, ...rest }) => {
  return <StyledBtn {...rest} color={color} variant={variant} />
}

const redButtonContained = css`
  background-color: ${red[600]};
  color: white;
  &:hover {
    background-color: ${red[800]};
  }
`

const redButtonOutlined = css`
  border: 1px solid ${fade(red[600], 0.3)};
  background-color: white;
  color: ${red[600]};
  &:hover {
    background-color: ${fade(red[600], 0.03)};
    border: 1px solid ${red[600]};
  }
`

const renderRedButton = (variant) => {
  if (variant === 'outlined') return redButtonOutlined
  else return redButtonContained
}

const StyledBtn = styled(MaterialBtn)`
  ${({ color, variant }) => (color === 'red' ? renderRedButton(variant) : '')};
`

export default Btn
