import styled from 'styled-components'
import { MenuItem, fade } from '@material-ui/core'
import { PALETTE } from 'components/theme'

const Item = styled(MenuItem)`
  :hover {
    background-color: ${fade(PALETTE.primary.main, 0.2)};
  }
`
export default Item
