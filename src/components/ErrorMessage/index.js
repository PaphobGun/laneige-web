import React from 'react'
import { Typography } from '@material-ui/core'
import styled from 'styled-components'

const ErrorMessage = ({ message, ...rest }) => {
  return (
    <Wrapper {...rest}>
      <Typography variant='caption' color='error'>
        {message}
      </Typography>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  text-align: center;
  height: 30px;
`

export default ErrorMessage
