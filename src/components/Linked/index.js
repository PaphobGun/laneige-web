import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { PALETTE } from 'components/theme'

export const Linked = styled(Link)`
  text-decoration: none;
  :active {
    color: ${PALETTE.primary.main};
  }
`
