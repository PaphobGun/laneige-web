import React, { useEffect } from 'react'
import { Modal, Typography, makeStyles } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: '70vw',
    maxWidth: '375px',
    backgroundColor: '#fff',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: '50%',
    left: '50%',
    borderRadius: '4px',
    transform: 'translate(-50%, -50%)',
  },
}))

const ErrorModal = ({ onClose }) => {
  const [open, setOpen] = React.useState(false)
  const classes = useStyles()
  const handleClose = () => {
    setOpen(false)
    onClose()
  }

  useEffect(() => {
    setOpen(true)
  }, [])

  return (
    <Modal open={open} onClose={handleClose} disableAutoFocus disableEnforceFocus>
      <div className={classes.paper}>
        <Typography variant='h4' color='textPrimary'>
          Server error
        </Typography>
      </div>
    </Modal>
  )
}

export default ErrorModal
