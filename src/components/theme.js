import { createMuiTheme } from '@material-ui/core'

export const PALETTE = {
  primary: {
    main: '#9882c5',
    // contrastText: '#403866',
  },
  secondary: {
    main: '#48c3e0',
    contrastText: '#fff',
  },
  text: {
    primary: '#403866',
  },
  error: {
    // main: '#c80000',
    main: '#BA0729',
  },
}

const theme = createMuiTheme({
  palette: PALETTE,
  typography: {
    fontFamily: '"Ubuntu"',
  },
})

export default theme
