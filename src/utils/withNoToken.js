import React from 'react'
import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import localToken from './localToken'

const isExpired = (exp) => Date.now() >= exp * 1000

const withNoToken = (WrappedComponent) => (props) => {
  const { isLogin, isInitial } = useSelector((state) => state.auth)
  const { isLoading } = useSelector((state) => state.loading)
  const { token, exp } = localToken.getToken()
  if (isInitial) {
    if (!token || isExpired(exp) || !isLogin || isLoading) {
      return <WrappedComponent {...props} />
    } else {
      return <Redirect to='/' />
    }
  } else {
    return null
  }
}

export default withNoToken
