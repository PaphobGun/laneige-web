export default {
  setToken: ({ token, exp }) => {
    localStorage.setItem('token', token)
    localStorage.setItem('exp', exp)
  },
  getToken: () => ({
    token: localStorage.getItem('token'),
    exp: localStorage.getItem('exp'),
  }),
  clearToken: () => {
    localStorage.removeItem('token')
    localStorage.removeItem('exp')
  },
}
