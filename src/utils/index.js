export const checkError = {
  isClient: (res) => res && res.status >= 400 && res.status < 500,
}

export const formatError = (err) => {
  const isClientErr = checkError.isClient(err)
  return { err: { isClientErr, errMsg: err?.data?.message } }
}

export const mockApi = (data) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve({ data: { data } })
    }, 200)
  })
