import React from 'react'
import { useEffect } from 'react'
import { LOGIN_SUCCESS, LOGIN_FAILURE } from 'modules/Auth/reducer'
import { useDispatch } from 'react-redux'
import localToken from 'utils/localToken'

const isExpired = (exp) => Date.now() >= exp * 1000

const WithToken = ({ children }) => {
  const dispatch = useDispatch()
  const { token, exp } = localToken.getToken()

  useEffect(() => {
    if (token && !isExpired(exp)) {
      dispatch({ type: LOGIN_SUCCESS })
    } else {
      dispatch({ type: LOGIN_FAILURE })
    }
  }, [dispatch, exp, token])

  return <>{children}</>
}

export default WithToken
