import { useMediaQuery } from '@material-ui/core'

const useIsDesktop = () => {
  const isDesktop = useMediaQuery('(min-width: 600px)')
  return isDesktop
}

export default useIsDesktop
