import Axios from 'axios'
import { BASE_URL } from 'configs'
import localToken from './localToken'

class Client {
  constructor() {
    this.client = Axios.create({ baseURL: BASE_URL })
    this.client.interceptors.request.use(
      function (config) {
        const { token } = localToken.getToken()
        config.headers.Authorization = `Bearer ${token}`
        return config
      },
      function (error) {
        // Do something with request error
        return Promise.reject(error)
      }
    )
  }

  get = (url) => {
    return this.request({ method: 'GET', url })
  }

  post = (url, data) => {
    return this.request({ method: 'POST', url, data })
  }

  request = async (config) => {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await this.client(config)
        resolve(res)
      } catch (err) {
        reject(err.response)
      }
    })
  }
}

export default new Client()
