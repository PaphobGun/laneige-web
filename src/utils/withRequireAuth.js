import React from 'react'
import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import localToken from './localToken'

const isExpired = (exp) => Date.now() >= exp * 1000

const withRequireAuth = (WrappedComponent) => (props) => {
  const { isLogin, isInitial } = useSelector((state) => state.auth)
  const { token, exp } = localToken.getToken()
  if (isInitial) {
    if (!token || isExpired(exp) || !isLogin) {
      return <Redirect to='/login' />
    } else {
      return <WrappedComponent {...props} />
    }
  } else {
    return null
  }
}

export default withRequireAuth
